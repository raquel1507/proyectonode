var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');
const mongoose = require('mongoose');
var flash = require('connect-flash');
var passport = require('passport');
var fileupload = require('express-fileupload');

//CONEXION
var dir_bd = 'mongodb://localhost/producto_bd';
var mongodb = process.env.MONGODB_URI || dir_bd;

mongoose.connect(mongodb, {useNewUrlParser:true}, (error)=>{
  if(error){
    console.error(error);
  }else{
    console.log('Conexión establecida correctamente!!');
  }
});

mongoose.Promise = global.Promise;
var connect_db = mongoose.connection;
connect_db.on('error', console.error.bind(console, 'mongodb error: '));

//controladores
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var pruebaRouter = require('./routes/prueba');
var categoriaRouter = require('./routes/categoria');

var app = express();
require('./config/passport/passport.js')(passport);

app.use(session({
  key: 'Terry_sid',
  secret: 'terry1507',
  resave: false,
  saveUninitialized: false,
}));

app.use(passport.initialize());
app.use(passport.session());

app.use(flash());
app.use(fileupload());


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//controladores
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/prueba', pruebaRouter);
app.use('/categoria', categoriaRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
