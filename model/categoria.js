var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var categoria = new Schema ({
  nombre: {
    type: String,
    required: true
  },
  descripcion: {
    type: String,
    required: true
  },

  imagen: {
    type: String,
    required: true
  },

  //Obligatorias
  estado: {
    type: Boolean,
    default: true
  },
  creacion: {
    type: Date,
    default: new Date()
  },
  actualizacion: {
    type: Date,
    default: new Date()
  }
});

module.exports = mongoose.model('Categoria', categoria);
