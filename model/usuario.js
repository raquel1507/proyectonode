var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var usuario = new Schema ({
  nombre: {
    type: String,
    required: true
  },
  apellido: {
    type: String,
    required: true
  },
  cedula: {
    type: String,
    required: true
  },
  correo: {
    type: String,
    required: true
  },
  direccion: {
    type: String,
    required: true
  },
  fecha_nacimiento: {
    type: Date,
    required: true
  },
  edad: {
    type: Number,
    required: true
  },
  contrasena: {
    type: String,
    required: true
  },

  //Obligatorias
  estado: {
    type: Boolean,
    default: true
  },
  creacion: {
    type: Date,
    default: new Date()
  },
  actualizacion: {
    type: Date,
    default: new Date()
  }
});

module.exports = mongoose.model('Usuario', usuario);
