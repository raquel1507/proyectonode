var mongoose = require('mongoose');
require('./categoria');
var Categoria = mongoose.model('Categoria');
var Schema = mongoose.Schema;
var producto = new Schema ({
  nombre: {
    type: String,
    required: true
  },
  precio:{
    type: Number,
    resquired: true
  },
  descripcion:{
    type: String
  },
  stock: {
    type: Number,
    required: true
  },

  categoria:{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Categoria'
  },

  imagen: {
    type: String,
    required: true
  },
  //Obligatorias
  estado: {
    type: Boolean,
    default: true
  },
  creacion: {
    type: Date,
    default: new Date()
  },
  actualizacion: {
    type: Date,
    default: new Date()
  }
});

module.exports = mongoose.model('Producto', producto);
