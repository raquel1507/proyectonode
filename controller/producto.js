'use strict'
var fs = require('fs');

const Producto = require('../model/producto');
const Categoria = require('../model/categoria'); //

exports.test = function (req, res, next){
  res.send('Esto es un test');
}

exports.crear = function(req, res, next){
    if(req.body.nombre && req.body.precio && req.body.stock && req.files.imagen){

      let file = req.files.imagen;
      let ruta = __dirname.replace('controller', 'public/images/productos/');
      fs.mkdirSync(ruta, {
        recursive: true
      }, function(error_mkdir){
        if (error_mkdir) {
          console.error(error_mkdir);
          req.flash('alerta', 'Error al crear la carpeta!');
          res.redirect('/nuevo');
        }
      });
      fs.writeFile(ruta + ".gitignore", "*", (err) => {
        if (err) throw err;
        console.log('The file has been saved!');
      });
      file.mv(ruta + file.name, function(error_img){
        if (error_img) {
          console.error(error_img);
          req.flash('alerta', 'Error al cargar la imagen!');
          res.redirect('/nuevo');
        }else{
          req.body.imagen = '/images/productos/' + file.name;
          new Producto(req.body).save(error =>{
            if (error) {
              console.error(error);
              req.flash('alerta', 'Error al enviar los datos!');
              res.redirect('/nuevo');
            }else{
              req.flash('alerta', 'Datos guardados!');
              res.redirect('/lista');
            }
          });

        }
      });
    }else{
      req.flash('alerta', 'Los datos no se estan enviando correctamente!');
      res.redirect('/nuevo');
    }
}

exports.listar = function(req, res, next){
  if (req.isAuthenticated()) {
  Producto.find({}).populate('categoria').exec( function(error, lista){//
    if (error) {
      console.error(error);
      req.flash('alerta', 'Error al obtener los datos!');
      res.redirect('/');
    }else{
      if (lista && lista.length > 0) {
        req.flash;
        res.render('producto/lista', {
          title: 'Lista de productos',
          alert: req.flash('alerta'),
          obj_datos: req.user,
          lista: lista
        });
      }else {
        req.flash('alerta', 'No hay registros nuevos para mostrar!');
        res.redirect('/nuevo');
      }
    }
  });
}else {
  req.flash('alerta', 'Inicie sesión para poder acceder!');
  res.redirect('/');
}
};
exports.editar_bd = function(req, res, next){
  if (req.files && req.files.imagen) {
    let file = req.files.imagen;
    let ruta = __dirname.replace('controller', 'public/images/productos/');
    fs.mkdirSync(ruta, {
      recursive: true
    }, function(error_mkdir){
      if (error_mkdir) {
        console.error(error_mkdir);
        req.flash('alerta', 'Error al crear la carpeta!');
        res.redirect('/nuevo');
      }
    });
    fs.writeFile(ruta + ".gitignore", "*", (err) => {
      if (err) throw err;
      console.log('The file has been saved!');
    });
    file.mv(ruta + file.name, function(error_img){
      if (error_img) {
        console.error(error_img);
        req.flash('alerta', 'Error al cargar la imagen!');
        res.redirect('/nuevo');
      }else{
        req.body.imagen = '/images/productos/' + file.name;
        editar_producto(req, res);
      }
    });

  }else {
    editar_producto(req, res);
  }
};
var editar_producto = function(req, res){
  Producto.findOne({_id: req.params.id}, function(error, objeto_producto){
    if (error) {
      console.error(error);
      req.flash('alerta', 'Error al obtener los datos!');
      res.redirect('/lista');
    }else{
      if (objeto_producto) {
        req.body.actualizacion = new Date();
        objeto_producto.update(req.body, (error_actualizar)=>{
          if (error_actualizar) {
            console.error(error);
            req.flash('alerta', 'Error al obtener los datos!');
            res.redirect('/lista');
          }else{
            req.flash('alerta', 'Datos actualizados!');
            res.redirect('/lista');
          }
        });
      }else {
        req.flash('alerta', 'No existe el producto!');
        res.redirect('/nuevo');
      }
    }
  });
}
exports.editar_vista = function(req, res, next){
  Producto.findOne({_id: req.params.id}, function(error, objeto_producto){
    if (error) {
      console.error(error);
      req.flash('alerta', 'Error al obtener los datos!');
      res.redirect('/lista');
    }else{
      if (objeto_producto) {
        Categoria.find({estado: true}, function(error, lista){
          if (error) {
            console.error(error);
            req.flash('alerta', 'Error al obtener las categorias!');
            res.redirect('/nuevo');
          }else {
            req.flash;
            res.render('producto/nuevo', {
              title: 'Nuevo producto',
              alert: req.flash('alerta'),
              obj_datos: req.user,
              producto: objeto_producto,
              categoria: lista
              });
          }
        });

      }else {
        req.flash('alerta', 'No existe el producto!');
        res.redirect('/nuevo');
      }
    }
  });
};

exports.dar_baja = function(req, res, next){
  Producto.findOne({_id: req.params.id}, function(error, objeto_producto){
    if (error) {
      console.error(error);
      res.send({
        mensaje: 'Error al obtener los datos!',
        respuesta: null
      });
    }else{
      if (objeto_producto) {
        objeto_producto.update({
          estado: (objeto_producto.estado)? false : true,
          actualizacion: new Date()
        }, (error_actualizar)=>{
          if (error_actualizar) {
            console.error(error);
            res.send({
              mensaje: 'Error al obtener los datos!',
              respuesta: null
            });
          }else{
            res.send({
              mensaje: 'Estado cambiado!',
              respuesta: objeto_producto
            });
          }
        });
      }else {
        res.send({
          mensaje: 'No existe el producto!',
          respuesta: null
        });
      }
    }
  });
};
exports.eliminar = function(req, res, next){
  Producto.findOne({_id: req.params.id}, function(error, objeto_producto){
    if (error) {
      console.error(error);
      res.send({
        mensaje: 'Ocurrio un error',
        respuesta: null
      });
    }else{
      if (objeto_producto) {
        objeto_producto.delete((error_actualizar)=>{
          if (error_actualizar) {
            console.error(error);
            res.send({
              mensaje: 'Error al obtener los datos!',
              respuesta: null
            });
          }else{
            Producto.find({}).populate('categoria').exec(function(error, lista){
              if (error) {
                console.error(error);
                res.send({
                  mensaje: 'Error al obtener los datos!',
                  respuesta: null
                });
              }else{
                if (lista && lista.length > 0) {
                  res.send({
                    mensaje: 'Producto eliminado correctamente!',
                    respuesta: lista
                  });
                }else {
                  res.send({
                    mensaje: 'No hay registros nuevos para mostrar!',
                    respuesta: null
                  });
                }
              }
            });

          }
        });
      }else {
        res.send({
          mensaje: 'No existe el producto!',
          respuesta: null
        });
      }
    }
  });
};

exports.nuevo = function(req, res, next) {
  if (req.isAuthenticated()) {
    Categoria.find({estado: true}, function(error, lista){
      if (error) {
        console.error(error);
        req.flash('alerta', 'Error al obtener las categorias!');
        res.redirect('/nuevo');
      }else {
        req.flash;
        res.render('producto/nuevo', {
          title: 'Nuevo producto',
          alert: req.flash('alerta'),
          obj_datos: req.user,
          producto: null,
          categoria: lista
        });
      }
    });
  }else {
    req.flash('alerta', 'Inicie sesión para poder acceder!');
    res.redirect('/');
  }
};

//CATÁLOGO
exports.listado = function(req, res, next){
  if (req.isAuthenticated()) {
    Producto.find({}).populate('categoria').exec( function(error, lista){//
      if (error) {
        console.error(error);
        req.flash('alerta', 'Error al obtener los datos!');
        res.redirect('/');
      }else{
        if (lista && lista.length > 0) {
          Categoria.find({estado: true}, function(error, lista_categoria){
            if (error) {
              console.error(error);
              req.flash('alerta', 'Error al obtener las categorias!');
              res.redirect('/nuevo');
            }else {
              req.flash;
              res.render('producto/catalogo', {
                title: 'Lista de productos',
                alert: req.flash('alerta'),
                obj_datos: req.user,
                lista: lista,
                categoria: lista_categoria
              });
            }
          });

        }else {
          req.flash('alerta', 'No hay registros nuevos para mostrar!');
          res.redirect('/nuevo');
        }
      }
    });
  }else {
    req.flash('alerta', 'Inicie sesión para poder acceder!');
    res.redirect('/');
  }
};
