'use strict'

const Usuario = require('../model/usuario');


exports.test = function (req, res, next){
  res.send('Esto es un test');
}

exports.crear = function(req, res, next){
  if(req.body.nombre && req.body.apellido && req.body.cedula && req.body.correo && req.body.direccion && req.body.fecha_nacimiento && req.body.edad && req.body.contrasena){
    /*
    var datos = {
      nombre: req.body.nombre,
      apellido: req.body.apellido,
      cedula: req.body.cedula,
      descripcion: req.body.descripcion
    };
    */
    req.body.fecha_nacimiento = new Date(req.body.fecha_nacimiento);
    new Usuario(req.body).save(error =>{
      if (error) {
        console.error(error);
        req.flash('alerta', 'Error al enviar los datos!');
        res.redirect('/users/nuevo');
      }else{
        req.flash('alerta', 'Datos guardados!');
        res.redirect('/users/lista');
      }
    });
  }else{
    req.flash('alerta', 'Los datos no se estan enviando correctamente!');
    res.redirect('/users/nuevo');
  }
}

exports.listar = function(req, res, next){
  if (req.isAuthenticated()) {
    Usuario.find({}, function(error, lista){
      if (error) {
        console.error(error);
        req.flash('alerta', 'Error al obtener los datos!');
        res.redirect('/');
      }else{
        if (lista && lista.length > 0) {
          req.flash;
          res.render('usuario/lista', {
            title: 'Lista de usuarios',
            alert: req.flash('alerta'),
            obj_datos: req.user,
            lista: lista
          });
        }else {
          req.flash('alerta', 'No hay registros nuevos para mostrar!');
          res.redirect('/users/nuevo');
        }
      }
    });

  }else {
    req.flash('alerta', 'Inicie sesión para poder acceder!');
    res.redirect('/');
  }
};
exports.editar_bd = function(req, res, next){
  Usuario.findOne({_id: req.params.id}, function(error, objeto_usuario){
    if (error) {
      console.error(error);
      req.flash('alerta', 'Error al obtener los datos!');
      res.redirect('/users/lista');
    }else{
      if (objeto_usuario) {
        req.body.actualizacion = new Date();
        objeto_usuario.update(req.body, (error_actualizar)=>{
          if (error_actualizar) {
            console.error(error);
            req.flash('alerta', 'Error al obtener los datos!');
            res.redirect('/users/lista');
          }else{
            req.flash('alerta', 'Datos actualizados!');
            res.redirect('/users/lista');
          }
        });
      }else {
        req.flash('alerta', 'No existe el usuario!');
        res.redirect('/users/nuevo');
      }
    }
  });
};
exports.editar_vista = function(req, res, next){
  Usuario.findOne({_id: req.params.id}, function(error, objeto_usuario){
    if (error) {
      console.error(error);
      req.flash('alerta', 'Error al obtener los datos!');
      res.redirect('/users/lista');
    }else{
      if (objeto_usuario) {
        req.flash;
        res.render('usuario/nuevo', {
          title: 'Nuevo usuario',
          alert: req.flash('alerta'),
          obj_datos: req.user,
          usuario: objeto_usuario
        });
      }else {
        req.flash('alerta', 'No existe el usuario!');
        res.redirect('/users/nuevo');
      }
    }
  });
};

exports.dar_baja = function(req, res, next){
  Usuario.findOne({_id: req.params.id}, function(error, objeto_usuario){
    if (error) {
      console.error(error);
      res.send({
        mensaje: 'Error al obtener los datos!',
        respuesta: null
      });
    }else{
      if (objeto_usuario) {
        objeto_usuario.update({
          estado: (objeto_usuario.estado)? false : true,
          actualizacion: new Date()
        }, (error_actualizar)=>{
          if (error_actualizar) {
            console.error(error);
            res.send({
              mensaje: 'Error al obtener los datos!',
              respuesta: null
            });
          }else{
            res.send({
              mensaje: 'Estado cambiado!',
              respuesta: objeto_usuario
            });
          }
        });
      }else {
        res.send({
          mensaje: 'No existe el usuario!',
          respuesta: null
        });
      }
    }
  });
};
exports.eliminar = function(req, res, next){
  Usuario.findOne({_id: req.params.id}, function(error, objeto_usuario){
    if (error) {
      console.error(error);
      res.send({
        mensaje: 'Ocurrio un error',
        respuesta: null
      });
    }else{
      if (objeto_usuario) {
        objeto_usuario.delete((error_actualizar)=>{
          if (error_actualizar) {
            console.error(error);
            res.send({
              mensaje: 'Error al obtener los datos!',
              respuesta: null
            });
          }else{
            Usuario.find({}, function(error, lista){
              if (error) {
                console.error(error);
                res.send({
                  mensaje: 'Error al obtener los datos!',
                  respuesta: null
                });
              }else{
                if (lista && lista.length > 0) {
                  res.send({
                    mensaje: 'Usuario eliminado correctamente!',
                    respuesta: lista
                  });
                }else {
                  res.send({
                    mensaje: 'No hay registros nuevos para mostrar!',
                    respuesta: null
                  });
                }
              }
            });

          }
        });
      }else {
        res.send({
          mensaje: 'No existe el usuario!',
          respuesta: null
        });
      }
    }
  });
};
