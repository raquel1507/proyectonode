'use strict'
var fs = require('fs');

const Categoria = require('../model/categoria');

exports.test = function (req, res, next){
  res.send('Esto es un test');
}

exports.crear = function(req, res, next){
  if(req.body.nombre && req.body.descripcion && req.files.imagen){
    let file = req.files.imagen;
    let ruta = __dirname.replace('controller', 'public/images/categorias/');
    fs.mkdirSync(ruta, {
      recursive: true
    }, function(error_mkdir){
      if (error_mkdir) {
        console.error(error_mkdir);
        req.flash('alerta', 'Error al crear la carpeta!');
        res.redirect('/categoria/nuevo');
      }
    });
    fs.writeFile(ruta + ".gitignore", "*", (err) => {
      if (err) throw err;
      console.log('The file has been saved!');
    });
    file.mv(ruta + file.name, function(error_img){
      if (error_img) {
        console.error(error_img);
        req.flash('alerta', 'Error al cargar la imagen!');
        res.redirect('/categoria/nuevo');
      }else{
        req.body.imagen = '/images/categorias/' + file.name;
        new Categoria(req.body).save(error =>{
          if (error) {
            console.error(error);
            req.flash('alerta', 'Error al enviar los datos!');
            res.redirect('/categoria/nuevo');
          }else{
            req.flash('alerta', 'Datos guardados!');
            res.redirect('/categoria/lista');
          }
        });

      }
    });
  }else{
    req.flash('alerta', 'Los datos no se estan enviando correctamente!');
    res.redirect('/categoria/nuevo');
  }
}

exports.listar = function(req, res, next){
  if (req.isAuthenticated()) {
    Categoria.find({}, function(error, lista){
      if (error) {
        console.error(error);
        req.flash('alerta', 'Error al obtener los datos!');
        res.redirect('/');
      }else{
        if (lista && lista.length > 0) {
          req.flash;
          res.render('categoria/lista', {
            title: 'Lista de categorias',
            alert: req.flash('alerta'),
            obj_datos: req.user,
            lista: lista
          });
        }else {
          req.flash('alerta', 'No hay registros nuevos para mostrar!');
          res.redirect('/categoria/nuevo');
        }
      }
    });
  }else {
    req.flash('alerta', 'Inicie sesión para poder acceder!');
    res.redirect('/');
  }
};
exports.editar_bd = function(req, res, next){
  if (req.files && req.file.imagen) {
    let file = req.files.imagen;
    let ruta = __dirname.replace('controller', 'public/images/categorias/');
    fs.mkdirSync(ruta, {
      recursive: true
    }, function(error_mkdir){
      if (error_mkdir) {
        console.error(error_mkdir);
        req.flash('alerta', 'Error al crear la carpeta!');
        res.redirect('/categoria/nuevo');
      }
    });
    fs.writeFile(ruta + ".gitignore", "*", (err) => {
      if (err) throw err;
      console.log('The file has been saved!');
    });
    file.mv(ruta + file.name, function(error_img){
      if (error_img) {
        console.error(error_img);
        req.flash('alerta', 'Error al cargar la imagen!');
        res.redirect('/categoria/nuevo');
      }else{
        req.body.imagen = '/images/categorias/' + file.name;
        editar_categoria(req, res);
      }
    });
  }else {
    editar_categoria(req, res);
  }
};

var editar_categoria = function(req, res){
  Categoria.findOne({_id: req.params.id}, function(error, objeto_categoria){
    if (error) {
      console.error(error);
      req.flash('alerta', 'Error al obtener los datos!');
      res.redirect('/categoria/lista');
    }else{
      if (objeto_categoria) {
        req.body.actualizacion = new Date();
        objeto_categoria.update(req.body, (error_actualizar)=>{
          if (error_actualizar) {
            console.error(error);
            req.flash('alerta', 'Error al obtener los datos!');
            res.redirect('/categoria/lista');
          }else{
            req.flash('alerta', 'Datos actualizados!');
            res.redirect('/categoria/lista');
          }
        });
      }else {
        req.flash('alerta', 'No existe el categoria!');
        res.redirect('/categoria/nuevo');
      }
    }
  });
}
exports.editar_vista = function(req, res, next){
  Categoria.findOne({_id: req.params.id}, function(error, objeto_categoria){
    if (error) {
      console.error(error);
      req.flash('alerta', 'Error al obtener los datos!');
      res.redirect('/categoria/lista');
    }else{
      if (objeto_categoria) {
        req.flash;
        res.render('categoria/nuevo', {
          title: 'Nuevo categoria',
          alert: req.flash('alerta'),
          obj_datos: req.user,
          categoria: objeto_categoria
        });
      }else {
        req.flash('alerta', 'No existe el categoria!');
        res.redirect('/categoria/nuevo');
      }
    }
  });
};

exports.dar_baja = function(req, res, next){
  Categoria.findOne({_id: req.params.id}, function(error, objeto_categoria){
    if (error) {
      console.error(error);
      res.send({
        mensaje: 'Error al obtener los datos!',
        respuesta: null
      });
    }else{
      if (objeto_categoria) {
        objeto_categoria.update({
          estado: (objeto_categoria.estado)? false : true,
          actualizacion: new Date()
        }, (error_actualizar)=>{
          if (error_actualizar) {
            console.error(error);
            res.send({
              mensaje: 'Error al obtener los datos!',
              respuesta: null
            });
          }else{
            res.send({
              mensaje: 'Estado cambiado!',
              respuesta: objeto_categoria
            });
          }
        });
      }else {
        res.send({
          mensaje: 'No existe el categoria!',
          respuesta: null
        });
      }
    }
  });
};
exports.eliminar = function(req, res, next){
  Categoria.findOne({_id: req.params.id}, function(error, objeto_categoria){
    if (error) {
      console.error(error);
      res.send({
        mensaje: 'Ocurrio un error',
        respuesta: null
      });
    }else{
      if (objeto_categoria) {
        objeto_categoria.delete((error_actualizar)=>{
          if (error_actualizar) {
            console.error(error);
            res.send({
              mensaje: 'Error al obtener los datos!',
              respuesta: null
            });
          }else{
            Categoria.find({}, function(error, lista){
              if (error) {
                console.error(error);
                res.send({
                  mensaje: 'Error al obtener los datos!',
                  respuesta: null
                });
              }else{
                if (lista && lista.length > 0) {
                  res.send({
                    mensaje: 'Categoria eliminado correctamente!',
                    respuesta: lista
                  });
                }else {
                  res.send({
                    mensaje: 'No hay registros nuevos para mostrar!',
                    respuesta: null
                  });
                }
              }
            });

          }
        });
      }else {
        res.send({
          mensaje: 'No existe el categoria!',
          respuesta: null
        });
      }
    }
  });
};

///////////////////////////////////////////////////
exports.nuevo = function(req, res, next) {
  if (req.isAuthenticated()) {
    Categoria.find({estado: true}, function(error, lista){
      if (error) {
        console.error(error);
        req.flash('alerta', 'Error al obtener las categorias!');
        res.redirect('/nuevo');
      }else {
        req.flash;
        res.render('categoria/nuevo', {
          title: 'Nueva categoria',
          alert: req.flash('alerta'),
          obj_datos: req.user,
          producto: null,
          categoria: lista
        });
      }
    });
  }else {
    req.flash('alerta', 'Inicie sesión para poder acceder!');
    res.redirect('/');
  }
};
