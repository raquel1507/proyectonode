const Usuario = require('../../model/usuario');
const bcryp = require('bcrypt-nodejs');

module.exports = function (passport) {

  var LocalStrategy = require('passport-local').Strategy;
  var LocalAPIKeyStrategy = require('passport-localapikey').Strategy;

  passport.serializeUser(function (account, done) {
    done(null, account);
  });

  // used to deserialize the user
  passport.deserializeUser(function (id, done) {
    if(id){
      //aqui se deserializa el usuario para mantener su informacion
      done(null, id);
    } else {
      done(id.errors, null);
    }
  });
  //registro de usuarios por passport
  passport.use('local-signup', new LocalStrategy(
    {
      usernameField: 'correo',//lo que esta como name en el input del registro
      passwordField: 'contrasena',//lo que esta como name en el input del registro
      passReqToCallback: true // allows us to pass back the entire request to the callback
    },
    function (req, email, password, done) {
      var generateHash = function(password){
        return bcryp.hashSync(password, bcryp.genSaltSync(8), null);
      }
      if(req.body.nombre && req.body.apellido && req.body.cedula && req.body.correo && req.body.direccion && req.body.fecha_nacimiento && req.body.edad && req.body.contrasena){
        req.body.fecha_nacimiento = new Date(req.body.fecha_nacimiento);
        req.body.contrasena = generateHash(req.body.contrasena);
        new Usuario(req.body).save((error, newUser) =>{
          if (error) {
            console.error(error);
            req.flash('alerta', 'Error al enviar los datos!');
            done(null, false);
          }else{
            req.flash('alerta', 'Datos guardados!');

            done(null, newUser);
          }
        });
      }else{
        req.flash('alerta', 'Los datos no se están enviando correctamente!');
        console.log('Datos vacíos');
        done(null, false);
      }
    }
  ));
  //inicio de sesion
  passport.use('local-signin', new LocalStrategy(
    {
      usernameField: 'usuario',
      passwordField: 'contrasena',
      passReqToCallback: true // allows us to pass back the entire request to the callback
    },
    function (req, email, password, done) {
      var isValidPassword = function(clave_bd, contrasena){
        return bcryp.compareSync(contrasena, clave_bd);
      }
      Usuario.findOne({correo: req.body.usuario}, function(error, objeto_usuario){
        if (error) {
          console.error(error);
          req.flash('alerta', 'Ocorrió un error');
          done(null, false);
        }else{
          if (objeto_usuario) {
            if (isValidPassword(objeto_usuario.contrasena, req.body.contrasena)) {
              done(null, objeto_usuario);
            }else{
              req.flash('alerta', 'Clave inválida!');
              done(null, false);
            }

          }else {
            req.flash('alerta', 'Correo incorrecto!');
            done(null, false);
          }
        }
      });
    }
  ));
  //verificar correo
  passport.use(new LocalAPIKeyStrategy(
    function(apikey, done) {

    }
  ));
};
