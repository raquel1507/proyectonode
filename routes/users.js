var express = require('express');
var passport = require('passport');
var router = express.Router();
const controller = require('../controller/usuario');

/* GET users listing. */
router.get('/', function(req, res, next) {
  req.flash;
  res.render('usuario/principal', {
    title: 'Vista de usuarios',
    alert: req.flash('alerta'),
    obj_datos: req.user
  });
});
router.get('/lista', controller.listar);
router.get('/nuevo', function(req, res, next) {
  req.flash;
  res.render('usuario/nuevo', {
    title: 'Nuevo usuario',
    alert: req.flash('alerta'),
    obj_datos: req.user,
    usuario: null
    });
});
router.get('/login', function(req, res, next) {
  req.flash;
  res.render('usuario/login', {
    title: 'Login de usuario',
    alert: req.flash('alerta'),
    obj_datos: req.user,
    usuario: null
    });
});
router.get('/logout', function(req, res, next) {
  req.logout();
  req.session.destroy(function(err) {
       if (req.user) {
         req.user.login_first = false;
       }
       res.redirect('/');
   });
});
router.post('/crear', passport.authenticate('local-signup', {
  successRedirect: '/',
  failureRedirect: '/users/nuevo'
}));
router.post('/auth', passport.authenticate('local-signin', {
  successRedirect: '/',
  failureRedirect: '/users/nuevo'
}));
router.post('/editar_bd/:id', controller.editar_bd);
router.get('/editar_vista/:id', controller.editar_vista);
router.get('/dar_baja/:id', controller.dar_baja);
router.get('/eliminar/:id', controller.eliminar);

module.exports = router;
