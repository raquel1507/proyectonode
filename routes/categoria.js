var express = require('express');
var router = express.Router();
const controller = require('../controller/categoria');

router.get('/', function(req, res, next) {
  res.redirect('/categoria/lista');
});
router.get('/lista', controller.listar);
router.get('/nuevo', function(req, res, next) {
  req.flash;
  res.render('categoria/nuevo', {
    title: 'Nuevo categoria',
    alert: req.flash('alerta'),
    obj_datos: req.user,
    categoria: null
    });
});
router.post('/crear', controller.crear);
router.post('/editar_bd/:id', controller.editar_bd);
router.get('/editar_vista/:id', controller.editar_vista);
router.get('/dar_baja/:id', controller.dar_baja);
router.get('/eliminar/:id', controller.eliminar);


module.exports = router;
