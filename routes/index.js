var express = require('express');
var router = express.Router();
const controller = require('../controller/producto');

/* GET home page. */
router.get('/b', function(req, res, next) {
  req.flash;
  res.render('producto/borrador', {
    title: 'Catalogo de Productos',
    alert: req.flash('alerta'),
    obj_datos: req.user
  });
});
router.get('/', function(req, res, next) {
  req.flash;
  res.render('producto/principal', {
    title: 'Catalogo de Productos',
    alert: req.flash('alerta'),
    obj_datos: req.user
  });
});

router.get('/lista', controller.listar);
router.get('/catalogo', controller.listado);//Catálogo de Productos
router.get('/nuevo', controller.nuevo);
router.post('/crear', controller.crear);
router.post('/editar_bd/:id', controller.editar_bd);
router.get('/editar_vista/:id', controller.editar_vista);
router.get('/dar_baja/:id', controller.dar_baja);
router.get('/eliminar/:id', controller.eliminar);

module.exports = router;
