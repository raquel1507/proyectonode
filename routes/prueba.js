var express = require('express');

const controller = require('../controller/producto');

var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Prueba' });
});
router.get('/test', controller.test);

module.exports = router;


/*
var express = require('express');
var router = express.Router();
const controller = require('../controller/usuario');

router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});
router.get('/ruta2', function(req, res, next) {
  res.render('ruta2', { title: 'ruta2' });
});

*/
